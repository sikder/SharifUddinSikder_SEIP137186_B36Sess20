<?php

namespace App;

class Student extends Person{

    public $StudentID="SEIP 143372";

    public function showStudentInfo(){
        parent::showPersonInfo();
        echo "Student ID:".$this->StudentID;
    }

}