<?php

namespace App;

class Person
{
    public $name="Karim";
    public $gender="Male";
    public $blood_group="O+";

    public function showPersonInfo(){
        echo "Name:".$this->name."<br>";
        echo "Gender:".$this->gender."<br>";
        echo "Blood Group:".$this->blood_group."<br>";
    }
}